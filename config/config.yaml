#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################

# ========================================================
# Config file for RNAflow pipeline
#=========================================================

# Absolute path of directory where fastq are stored
input_dir: /path/to/your/data
# How mate paires are written in fastq as a regular expression
input_mate: '_R[12]'
# filemame extension
input_extension: '.fastq.gz'
# directory where you want 
analysis_dir: results
# tmpdir: write tempory file on this directory (could be /tmp/ or "/pasteur/appa/scratch/LOGIN/")
tmpdir: $MYSCRATCH


#===============================================================================
# Genome Section
# Note that this pipeline is conceived to used one or more genome. 
# Please always fill the four first lines.
#
# :Parameters:
#
# - genome_directory: directory where all indexed are written
# - name: name of prefix use in all output files from mapping [First Genome]
# - fasta_file: path to Reference Genome in fasta format [First Genome]
# - gff_file : path to annotation file  [First Genome]
# - host_mapping: set to yes if you want align reads also against another genome [Second Genome]
# - host_name: name of prefix use in all output files from mapping related to [Second Genome]
# - host_fasta_file: path related to another genome in fasta format [Second Genome]
# - gff_file : path to annotation file [Second Genome]
# - rRNA_mapping: Mapping on ribosomal RNA. If yes, sortmeRNA is used to estimate ribosomal rate
# - ribo_fasta_file : path to ribosomal sequences in fasta format
#===============================================================================


genome:
    genome_directory: /pasteur/zeus/projets/p01/BioIT/Genomes
    name: saccer3
    fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/saccer3/saccer3.fa
    gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/saccer3/saccer3.gff
    host_mapping: yes
    host_name: hg38
    host_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.fa
    host_gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.gff
    rRNA_mapping: yes
    ribo_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_rRNA.fa  

#===============================================================================
# FastQC section
#
# :Parameters:
#
# - options: string with any valid FastQC options
#===============================================================================

fastqc:
    options: --nogroup
    threads: 4   


#===============================================================================
# Fastq_screen section
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - conf: string with any valid FastQC options
#===============================================================================

fastq_screen:
    do: yes
    conf: /pasteur/zeus/projets/p01/BioIT/Genomes/fastq_screen_conf/fastq_screen.conf
    threads: 4 



#===============================================================================
# Quality trimming and adapter removal
#
# :Parameters:
#
# - remove: skip this step if data are clean
# - tool: could be "cutadapt" or "alienTrimmer"
# - adapter_list: a string or file (prefixed with *file:*) for cutadapt
# - alien_file: the alien file for alienTrimmer
# - m: 30 means discard trimmed reads that are shorter than 30.
# - mode: could be g for 5', a for 3', b for both 5'/3'
# - options: See cutadapt documentation for details. For paired-end reads,
#            specific options must be added here (except -p option, automatically
#            used in the rule for them). Default options are set for each tool,
#            please comment the line you doesn't use
# - quality: 0 means no trimming, 30 means keep base with quality
#        above 30
# - threads: number of threads to use 
#
#===============================================================================


adapters:
    remove: yes
    tool: cutadapt # could be alienTrimmer or cutadapt
    cutadapt_file: file:config/TruSeq_Stranded_RNA.fa
    alien_file: config/TruSeq_Stranded_RNA.fa
    m: 25
    mode: b
    options_cutadapt: -O 6 --trim-n --max-n 1 
    options_alien: -k 10 -m 5 -p 80  
    quality: 30
    threads: 4

#===============================================================================
# bowtie2_mapping used to align reads against genome file
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - options: any options recognised by bowtie2 tool
# - threads: number of threads to be used
#===============================================================================


bowtie2_mapping:
    do: yes
    options: "--very-sensitive "
    threads: 4

#===============================================================================
# star_mapping used to align reads against genome file
#
# :Parameters:
#
# - options: any options recognised by bowtie2 tool. For small genome, STAR may 
#            abort with segmentation fault. Setting sjdb Over hang to 250 skips this issue
# - pass2: Splice junctions used during pass2 of STAR. Could be "by-sample", "all-samples", 
#          "none". If "none", no second pass will be done.
# - threads: number of threads to be used
#===============================================================================


star_mapping:
    do: yes
    options: "--outFilterMismatchNoverLmax 0.05"
    pass2: "all-samples"
    threads: 4


#===============================================================================
# Mapping with minimap2
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored 
# - options: any options recognised by minimap2 tool
# - threads: number of threads to be used
#===============================================================================


minimap2:
    do: no
    options: "-ax sr"
    threads: 4

#===============================================================================
# pseudo mapping with kallisto
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - fasta: Fasta file for the kallisto index to be used for quantification
# - gtf: GTF file for transcriptome information (required for --genomebam)
# - kmer: k-mer (odd) length (default: 31, max value: 31)
# - options: any options recognised by kallisto tool
# - threads: number of threads to be used
#===============================================================================


kallisto:
    do: no
    fasta: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_cDNA.fa
    gtf: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.gtf
    options: "--single -l 75 -s 15"
    kmer: 31 
    threads: 12


#===============================================================================
# Quantification with Salmon quant (only available with STAR mapping)
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - fasta: Fasta file for the kallisto index to be used for quantification
# - gtf: GTF file for transcriptome information (required for --genomebam)
# - libtype: the type of sequencing library from which the reads come
# - options: any options recognised by salmon
# - threads: number of threads to be used
#
#
#===============================================================================


salmon:
    do: yes
    fasta: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_cDNA.fa
    options: ""
    libtype: "A" 
    threads: 12



#===============================================================================
# feature_counts used to count reads against features
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - options: options related to the first genome
# - options_host : options related to the host genome
# - threads: number of threads to be used
#===============================================================================


feature_counts:
    do: yes
    options: "-t gene -g ID -s 1 " 
    options_host: "-t gene -g gene_id -s 1 "
    threads: 8

#===============================================================================
# mark duplicates (picard-tools) allows to mark PCR duplicate in BAM files
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - remove: If true do not write duplicates to the output file instead of writing
#       them with appropriate flags set.  Default value: no. 
# - threads: number of threads to be used
#===============================================================================

mark_duplicates:
    do: no
    remove: no
    threads: 4


#===============================================================================
# bamCoverage from Deeptools
#  see https://deeptools.readthedocs.io/en/develop/content/tools/bamCoverage.html
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - options: options related to deeptools
#   see https://deeptools.readthedocs.io/en/latest/content/feature/effectiveGenomeSize.html
#       for more information about effective Genome Size
# - threads: number of threads to be used
#===============================================================================


bamCoverage:
    do: yes
    options: "--filterRNAstrand forward --effectiveGenomeSize 120000 " 
    options_host: "--filterRNAstrand forward --effectiveGenomeSize 2913022398 " 
    igv_session: yes
    threads: 4


#===============================================================================
#   MultiQC aggregates results from bioinformatics analyses across many
#   samples into a single report.
#
# :Parameters:
#
# - options: any options recognised by multiqc
#===============================================================================


multiqc:
    options: " -f -x 02-Mapping/*/*/*init* -x .snakemake"
