#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################



import pandas as pd
from fnmatch import fnmatch
from re import sub, match
from itertools import repeat, chain
import os


#-------------------------------------------------------
# read config files

configfile: "config/config.yaml"
RULES = os.path.join("workflow", "rules")


#-------------------------------------------------------
# list of all files in the directory 'input_dir'

filenames = [f for f in os.listdir(config["input_dir"]) if match(r'.*'+config["input_mate"]+config["input_extension"]+'', f)] 

if not filenames :
    raise ValueError("Please provides input fastq files")
   
samples = [sub(config["input_mate"]+config["input_extension"], '', file) for file in filenames]

#-------------------------------------------------------
# paired-end data gestion
mate_pair = config["input_mate"]
rt1 = mate_pair.replace("[12]", "1")
rt2 = mate_pair.replace("[12]", "2")

R1 = [1 for this in filenames if rt1 in this]
R2 = [1 for this in filenames if rt2 in this]

if len(R2) == 0:
    paired = False
else:
    if R1 == R2:
        paired = True
    else:
        raise ValueError("Please provides single or paired files only")

# -----------------------------------------------
#initialize global variables


sample_dir = config["input_dir"]

analysis_dir = config["analysis_dir"]
if not os.path.exists(analysis_dir):
    os.makedirs(analysis_dir)

input_data = [sample_dir + "/{{SAMPLE}}{}{}".format(rt1,config["input_extension"])]
if paired:
    input_data += [sample_dir + "/{{SAMPLE}}{}{}".format(rt2,config["input_extension"])]    
# global variable for all output files
final_output = []


# -----------------------------------------------
#begin of the rules

#----------------------------------
# quality control
#----------------------------------

fastqc_input_fastq = input_data
fastqc_output_done = os.path.join(analysis_dir, "00-Fastqc/{{SAMPLE}}{}_fastqc.done".format(rt1))
fastqc_wkdir = os.path.join(analysis_dir, "00-Fastqc")
fastqc_log = os.path.join(analysis_dir, "00-Fastqc/logs/{{SAMPLE}}{}_fastqc_raw.log".format(rt1))
final_output.extend(expand(fastqc_output_done, SAMPLE=samples))
include: os.path.join(RULES, "fastqc.rules")


#----------------------------------
# remove adapters
#----------------------------------

if config["adapters"]["remove"] :
    adapter_tool = config["adapters"]["tool"]
    adapters_input_fastq = input_data
    adapters_wkdir = os.path.join(analysis_dir, "01-Trimming")
    adapters_output = [os.path.join(analysis_dir, "01-Trimming/{{SAMPLE}}{}_trim.fastq".format(rt1))]
    if paired:
        adapters_output += [os.path.join(analysis_dir, "01-Trimming/{{SAMPLE}}{}_trim.fastq".format(rt2))]

    # Set parameters
    adapters_mode = config["adapters"]["mode"]
    adapters_min  = config["adapters"]["m"]
    adapters_qual = config["adapters"]["quality"]
    adapters_log = os.path.join(analysis_dir, "01-Trimming/logs/{SAMPLE}_trim.txt")
    #final_output.extend(expand(adapters_output, SAMPLE=samples))
    if adapter_tool == "cutadapt" :
        adapters_options = config["adapters"]["options_cutadapt"]
        include: os.path.join(RULES, "cutadapt.rules")
    elif adapter_tool == "alienTrimmer" :
        adapters_options = config["adapters"]["options_alien"]
        include: os.path.join(RULES, "alienTrimmer.rules")
    else :
        raise ValueError("Please provides an implemented removal adapter tool: must be cutadapt or alienTrimer")
    

else:
    adapters_output = input_data
    

   
#-----------------------------------------
# fastq_screen on samples
#-----------------------------------------

if config["fastq_screen"]["do"] :  
    fastq_screen_input = adapters_output
    fastq_screen_outdir = os.path.join(analysis_dir, "02-Mapping/Fastq-screen/{SAMPLE}")
    fastq_screen_logs_err = os.path.join(analysis_dir, "02-Mapping/Fastq-screen/logs/{SAMPLE}_mapping.err")
    fastq_screen_logs_out = os.path.join(analysis_dir, "02-Mapping/Fastq-screen/logs/{SAMPLE}_mapping.out")
    final_output.extend(expand(fastq_screen_outdir, SAMPLE=samples))
    include: os.path.join(RULES, "fastq_screen.rules")



#-----------------------------------------
# Estimate ribosomal rate in samples
#-----------------------------------------

   
if config["genome"]["rRNA_mapping"] :
    sortmerna_input = adapters_output
    sortmerna_fasta = config["genome"]["ribo_fasta_file"]
    sortmerna_outfile_rRNA = [os.path.join(analysis_dir, "02-Mapping/Ribo/{{SAMPLE}}_rRNA{}.fastq".format(rt1))]
    sortmerna_outfile_no_rRNA = [os.path.join(analysis_dir, "02-Mapping/Ribo/{{SAMPLE}}_norRNA{}.fastq".format(rt1))]
    if paired:
        sortmerna_outfile_rRNA += [os.path.join(analysis_dir, "02-Mapping/Ribo/{{SAMPLE}}_rRNA{}.fastq".format(rt2))]
        sortmerna_outfile_no_rRNA += [os.path.join(analysis_dir, "02-Mapping/Ribo/{{SAMPLE}}_norRNA{}.fastq".format(rt2))]
    sortmerna_logs_err = os.path.join(analysis_dir, "02-Mapping/Ribo/logs/{SAMPLE}_mapping.err")
    sortmerna_logs_out = os.path.join(analysis_dir, "02-Mapping/Ribo/logs/{SAMPLE}_mapping.out")
    final_output.extend(expand(sortmerna_outfile_no_rRNA, SAMPLE=samples))
    include: os.path.join(RULES, "sortmerna.rules")
    adapters_output = sortmerna_outfile_no_rRNA





#----------------------------------
# genome gestion
#----------------------------------

ref = [ config["genome"]["name"] ]

if config["genome"]["host_mapping"]:
    ref += [ config["genome"]["host_name"] ]


def mapping_index(wildcards):
    if (wildcards.REF == config["genome"]["name"]):
        if config["salmon"]["do"] and not config["genome"]["host_mapping"]:
            return {"fasta": config["salmon"]["fasta"]}
        else:
            return {"fasta": config["genome"]["fasta_file"]}
    elif (wildcards.REF == config["genome"]["host_name"]):
        if config["salmon"]["do"]:
            return {"fasta": config["salmon"]["fasta"]}
        else:       
            return {"fasta": config["genome"]["host_fasta_file"]}

def bowtie2_mapping_index(wildcards):
    if (wildcards.REF == config["genome"]["name"]):
        return {"fasta": config["genome"]["fasta_file"]}
    elif (wildcards.REF == config["genome"]["host_name"]):
        return {"fasta": config["genome"]["host_fasta_file"]}

def minimap2_mapping_index(wildcards):
    if (wildcards.REF == config["genome"]["name"]):
        return config["genome"]["fasta_file"]
    elif (wildcards.REF == config["genome"]["host_name"]):
        return config["genome"]["host_fasta_file"]
  
def mapping_prefix(wildcards):
    if (wildcards.REF == config["genome"]["name"]):
        return {"prefix": config["genome"]["name"]}
    elif (wildcards.REF == config["genome"]["host_name"]):
        return {"prefix": config["genome"]["host_name"]}

  
def annot_index(wildcards):
    if (wildcards.REF == config["genome"]["name"]):
        if config["salmon"]["do"] and not config["genome"]["host_mapping"]:
            return {"gff_file": config["salmon"]["fasta"]}
        else:
            return {"gff_file": config["genome"]["gff_file"]}
    elif (wildcards.REF == config["genome"]["host_name"]):
        if config["salmon"]["do"]:
            return {"gff_file": config["salmon"]["fasta"]}
        else:
            return {"gff_file": config["genome"]["host_gff_file"]}


#----------------------------------
# bowtie2 MAPPING
#----------------------------------

mapper = []

if config["bowtie2_mapping"]["do"]: 
    mapper += ["bowtie2"]

    # indexing for bowtie2
    bowtie2_index_fasta = unpack(bowtie2_mapping_index)
    bowtie2_index_log = os.path.join(analysis_dir, "02-Mapping/{REF}/bowtie2/logs/bowtie2_{REF}_indexing.log")
    bowtie2_index_output_done = os.path.join(config["genome"]["genome_directory"], "{REF}/bowtie2/{REF}.1.bt2")
    bowtie2_index_output_prefix = os.path.join(config["genome"]["genome_directory"], "{REF}/bowtie2/{REF}")
    include: os.path.join(RULES, "bowtie2_index.rules")



    # Mapping step
    bowtie2_mapping_input = adapters_output
    bowtie2_mapping_index_done = bowtie2_index_output_done
    bowtie2_mapping_sort = os.path.join(analysis_dir, "02-Mapping/{REF}/bowtie2/{SAMPLE}_{REF}_sort.bam")
    bowtie2_mapping_bam = os.path.join(analysis_dir, "02-Mapping/{REF}/bowtie2/{SAMPLE}_{REF}.bam")
    bowtie2_mapping_sortprefix = os.path.join(analysis_dir, "02-Mapping/{REF}/bowtie2/{SAMPLE}_{REF}_sort")
    bowtie2_mapping_logs_err = os.path.join(analysis_dir, "02-Mapping/{REF}/bowtie2/logs/{SAMPLE}_{REF}_mapping.err")
    bowtie2_mapping_logs_out = os.path.join(analysis_dir, "02-Mapping/{REF}/bowtie2/logs/{SAMPLE}_{REF}_mapping.out")
    bowtie2_mapping_prefix_index = bowtie2_index_output_prefix
    bowtie2_mapping_options =  config["bowtie2_mapping"]["options"]
    final_output.extend(expand(bowtie2_mapping_sort, SAMPLE=samples, REF=ref))
    include: os.path.join(RULES, "bowtie2_mapping.rules")


#----------------------------------
# STAR MAPPING
#----------------------------------

if config["star_mapping"]["do"]: 
    mapper += ["STAR"]
    star_index_fasta = unpack(mapping_index)
    star_mapping_splice_file = unpack(annot_index)
    star_index_log = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/logs/STAR_{REF}_indexing.log")
    star_index_output_done = os.path.join(config["genome"]["genome_directory"], "{REF}/STAR/SAindex")
    star_index_output_dir = os.path.join(config["genome"]["genome_directory"], "{REF}/STAR/")
 
    include: os.path.join(RULES, "star_index.rules")

        
    #first pass mapping
    star_mapping_pass1_input = adapters_output
    star_mapping_pass1_done = star_index_output_done
    star_mapping_pass1_index = star_index_output_dir
    if config["star_mapping"]["pass2"] == "none":
        star_mapping_pass1_logs = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/logs/{SAMPLE}_{REF}.out")
        star_mapping_pass1_output_prefix =  os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_")
        star_mapping_pass1_junctions = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_SJ.out.tab")
        star_mapping_pass1_bam =  os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_Aligned.sortedByCoord.out.bam")
    else :
        star_mapping_pass1_logs = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/logs/{SAMPLE}_{REF}_init.out")
        star_mapping_pass1_output_prefix =  os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_init_")
        star_mapping_pass1_junctions = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_init_SJ.out.tab")
        star_mapping_pass1_bam =  os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_init_Aligned.sortedByCoord.out.bam")
    star_mapping_pass1_read_groups = ""
    #final_output.extend(expand(star_mapping_pass1_junctions, SAMPLE=samples, REF=ref))    
    include: os.path.join(RULES, "star_mapping_pass1.rules")  
    
    if config["star_mapping"]["pass2"] != "none":
        #Second pass mapping
        star_mapping_pass2_input = adapters_output
        star_mapping_pass2_done = star_index_output_done
        star_mapping_pass2_index = star_index_output_dir
        star_mapping_pass2_logs = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/logs/{SAMPLE}_{REF}.out")
        star_mapping_pass2_output_prefix =  os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_")
        if config["star_mapping"]["pass2"] == "by-sample":
            star_mapping_pass2_junctions = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_init_SJ.out.tab")
        elif config["star_mapping"]["pass2"] == "all-samples":
            star_mapping_pass2_junctions = expand(os.path.join(analysis_dir, "02-Mapping/{{REF}}/STAR/{SAMPLE}_{{REF}}_init_SJ.out.tab"), SAMPLE=samples)
        else:
            raise ValueError("Please provides a valid option to pass2 of STAR: must be by-sample, all-samples or none")
        star_mapping_pass2_bam = star_mapping_pass1_bam
        star_mapping_pass2_read_groups = "ID:{SAMPLE}"
        star_mapping_pass2_sort = os.path.join(analysis_dir, "02-Mapping/{REF}/STAR/{SAMPLE}_{REF}_Aligned.sortedByCoord.out.bam")

        final_output.extend(expand(star_mapping_pass2_sort, SAMPLE=samples, REF=ref))    
        include: os.path.join(RULES, "star_mapping_pass2.rules")    
  
#----------------------------------
# MINIMAP2 MAPPING
#----------------------------------


if config["minimap2"]["do"]: 
    mapper += ["minimap2"]

    minimap2_input = adapters_output
    minimap2_genome = minimap2_mapping_index
    minimap2_sort = os.path.join(analysis_dir, "02-Mapping/{REF}/minimap2/{SAMPLE}_{REF}_sort.bam")
    minimap2_bam = os.path.join(analysis_dir, "02-Mapping/{REF}/minimap2/{SAMPLE}_{REF}.bam")
    minimap2_logs_err = os.path.join(analysis_dir, "02-Mapping/{REF}/minimap2/logs/{SAMPLE}_{REF}_mapping.err")
    minimap2_logs_out = os.path.join(analysis_dir, "02-Mapping/{REF}/minimap2/logs/{SAMPLE}_{REF}_mapping.out")
    minimap2_options = config["minimap2"]["options"]
    final_output.extend(expand(minimap2_sort, SAMPLE=samples, REF=ref))
    include: os.path.join(RULES, "minimap2.rules")


#----------------------------------
# Mark duplicated reads
#----------------------------------
def output_mapping(wildcards):
    if (wildcards.MAP == "bowtie2"):
        input = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/{SAMPLE}_{REF}_sort.bam")
    elif (wildcards.MAP == "STAR"):
        input = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/{SAMPLE}_{REF}_Aligned.sortedByCoord.out.bam")
    elif (wildcards.MAP == "minimap2"):
        input = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/{SAMPLE}_{REF}_sort.bam")
    return(input)

#if mapping on ribosomal RNA done, do not perform markDuplicates, counting or coverage
if "Ribo" in ref:
    ref.pop()

if config["mark_duplicates"]["do"]:
    mark_duplicates_input = output_mapping
    mark_duplicates_output = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/{SAMPLE}_{REF}_sort_dedup.bam")
    mark_duplicates_metrics = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/{SAMPLE}_{REF}_sort_dedup.txt")
    mark_duplicates_log_std = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/logs/{SAMPLE}_{REF}_sort_dedup.out")
    mark_duplicates_log_err = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/logs/{SAMPLE}_{REF}_sort_dedup.err")
    mark_duplicates_tmpdir = config['tmpdir']
    final_output.extend(expand(mark_duplicates_output, SAMPLE=samples, REF=ref, MAP=mapper))
    include: os.path.join(RULES, "mark_duplicates.rules")



    
#----------------------------------  
# counting step
#----------------------------------

def counting_gff(wildcards):
    if (wildcards.REF == config['genome']['name']):
        input = config['genome']['gff_file']
    elif (wildcards.REF == config['genome']['host_name']):
        input = config['genome']['host_gff_file']
    return(input)

def counting_options(wildcards):
    if (wildcards.REF == config['genome']['name']):
        input = config['feature_counts']['options']
    elif (wildcards.REF == config['genome']['host_name']):
        input = config['feature_counts']['options_host']
    return(input)


if config["feature_counts"]["do"]: 
    if config["mark_duplicates"]["remove"]:
        feature_counts_input = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/{SAMPLE}_{REF}_sort_dedup.bam")
    else:
        feature_counts_input = output_mapping
    feature_counts_output_count = os.path.join(analysis_dir, "03-Counting/{REF}/{MAP}/{SAMPLE}_{REF}_feature.out")
    feature_counts_gff  = counting_gff
    feature_counts_logs_err = os.path.join(analysis_dir, "03-Counting/logs/{MAP}/{SAMPLE}_{REF}_feature.err")
    feature_counts_logs_out = os.path.join(analysis_dir, "03-Counting/logs/{MAP}/{SAMPLE}_{REF}_feature.out")
    feature_counts_options = counting_options
    final_output.extend(expand(feature_counts_output_count, SAMPLE=samples, REF=ref, MAP=mapper))
    include: os.path.join(RULES, "feature_counts.rules")



#----------------------------------
# Quantification with Salmon
#----------------------------------

if config["salmon"]["do"]: 
        
    #pseudo mapping
    salmon_quant_bam = star_mapping_pass2_sort
    salmon_quant_transcript = config["salmon"]["fasta"]
    salmon_quant_output_dir = os.path.join(analysis_dir, "03-Counting/salmon/{SAMPLE}_{REF}")
    salmon_quant_options = config["salmon"]["options"]
    salmon_quant_log_err = os.path.join(analysis_dir, "03-Counting/salmon/logs/{SAMPLE}_{REF}_salmon.err")
    salmon_quant_log_out = os.path.join(analysis_dir, "03-Counting/salmon/logs/{SAMPLE}_{REF}_salmon.out")
    final_output.extend(expand(salmon_quant_output_dir, SAMPLE=samples, REF=ref))
    include: os.path.join(RULES, "salmon_quant.rules")



#----------------------------------
# Samtools Flagstat
#----------------------------------

flagstat_input = output_mapping
flagstat_logs = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/logs/{SAMPLE}_{REF}.out")
flagstat_output = os.path.join(analysis_dir, "02-Mapping/{REF}/{MAP}/{SAMPLE}_{REF}_stats.out")
final_output.extend(expand(flagstat_output, SAMPLE=samples, REF=ref, MAP=mapper))    
include: os.path.join(RULES, "flagstat.rules")  

  
#----------------------------------
# Coverage step
#----------------------------------
if config["bamCoverage"]["do"]:
    bamCoverage_input = output_mapping
    bamCoverage_logs = os.path.join(analysis_dir, "04-Coverage/{REF}/{MAP}/logs/{SAMPLE}_{REF}.out")
    bamCoverage_output = os.path.join(analysis_dir, "04-Coverage/{REF}/{MAP}/{SAMPLE}_{REF}.bw")
    bamCoverage_options = config['bamCoverage']['options']
    final_output.extend(expand(bamCoverage_output, SAMPLE=samples, REF=ref, MAP=mapper))    
    include: os.path.join(RULES, "bamCoverage.rules")  

    if config["bamCoverage"]["igv_session"]:
        igv_session_input = expand(bamCoverage_output, SAMPLE=samples, REF=ref, MAP=mapper)
        igv_session_output = os.path.join(analysis_dir, "06-IGV/igv_session.xml")
        igv_session_genome = ref
        igv_session_wdir = os.path.join(analysis_dir, "06-IGV/")
        igv_session_autoScale = "True"
        igv_session_normalize = "False"
        igv_session_log_out = os.path.join(analysis_dir, "06-IGV/logs/igv_session.out")
        final_output.extend([igv_session_output])
        include: os.path.join(RULES, "igv_session.rules")

#----------------------------------
# PseudoMapping with Kallisto
#----------------------------------

if config["kallisto"]["do"]: 
    kallisto_index_fasta = config["kallisto"]["fasta"]
    kallisto_index_output = (os.path.splitext(config["kallisto"]["fasta"])[0])+".idx"
    kallisto_index_kmer = config["kallisto"]["kmer"]
    kallisto_index_log = os.path.join(analysis_dir, "02-Mapping/STAR/logs/Kallisto_indexing.log")
    include: os.path.join(RULES, "kallisto_index.rules")

        
    #pseudo mapping
    kallisto_quant_fastq = adapters_output
    kallisto_quant_index = (os.path.splitext(config["kallisto"]["fasta"])[0])+".idx"
    kallisto_quant_output_dir = os.path.join(analysis_dir, "02-Mapping/Kallisto/{SAMPLE}")
    kallisto_quant_options = config["kallisto"]["options"]
    kallisto_quant_gtf = config["kallisto"]["gtf"]
    kallisto_pseudo_log = os.path.join(analysis_dir, "02-Mapping/Kallisto/logs/{SAMPLE}_kallisto.log")
    final_output.extend(expand(kallisto_quant_output_dir, SAMPLE=samples))
    include: os.path.join(RULES, "kallisto_quant.rules")



#----------------------------------  
# MultiQC report
#----------------------------------
multiqc_input = final_output
multiqc_input_dir = analysis_dir
multiqc_logs = os.path.join(analysis_dir, "05-Multiqc/multiqc.log")
multiqc_output = os.path.join(analysis_dir, "05-Multiqc/multiqc_report.html")
multiqc_options = config['multiqc']['options'] + " -c config/multiqc_config.yaml" 
multiqc_output_dir = os.path.join(analysis_dir, "05-Multiqc")
final_output = [multiqc_output]
include: os.path.join(RULES, "multiqc.rules")


rule rnaflow:
    input: final_output
        

onsuccess:
    # remove file from first star mapping
    import os
    import glob
    import shutil
    for file in glob.glob('02-Mapping/*/STAR/*init*'):
        if os.path.isfile(file):
            os.remove(file)
        elif os.path.isdir(file):
            shutil.rmtree(file)
        else:    
            print("Error: %s file not found" % file)

    # move cluster log files
    pattern = re.compile("slurm.*")
    dest = "cluster_logs"
    for filepath in os.listdir("."):
        if pattern.match(filepath):
            if not os.path.exists(dest):
                os.makedirs(dest)
            shutil.move(filepath, dest)

