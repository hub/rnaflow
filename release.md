# RNAflow

## Authors

* Rachel Legendre (@rlegendr)


## Releases

31 Dec 2022:

- Optimize exon quantification is now available

Updates:

- Add salmon for quantification for eukaryotic genomes (https://doi.org/10.1371/journal.pone.0141910, https://doi.org/10.12688/f1000research.7563.2, https://doi.org/10.1016/j.csbj.2020.06.014)
- Mapping now starts from cleaned files (without rRNA) when sortmeRNA is set to yes
- Add IGV session from bigwig files
- Add fastq_screen
- genomeSAindexNbases and genomeChrBinNbits are automatically computed as desscribed in STAR manual
- connect output directory analysis


20 Nov 2021:

- Initial pipeline tested on Maestro and NNCR

Updates:

- Update version of sortmerna from 2.1b to 4.3.4 to take into account paired-end data
- Add options for second pass of STAR: junctions could be added by sample or from all samples as described by [Veeneman et al](https://doi.org/10.1093/bioinformatics/btv642) 
