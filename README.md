<img align="left" width="100" height="100" src="images/RNAflow_logo.png" alt="RNAflow">

# RNAflow: A snakemake-based pipeline for standard transcriptomic analysis

[![Snakemake](https://img.shields.io/badge/snakemake-≥5.2.1-brightgreen.svg)](https://snakemake.readthedocs.io/en/stable/) [![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)

## Table of content

[[_TOC_]] 


# What is RNAflow ?

RNAflow is a snakemake pipeline dedicated to standard transcriptomic analysis [@Hub of Bioinformatics & Biostatistics](https://research.pasteur.fr/en/team/bioinformatics-and-biostatistics-hub/).

Be careful, RNAflow was mainly developped to run on the HPC of Institut Pasteur, and not on other HPC.


<img src="images/rulegraph.svg" width="1000">


# How to install RNAflow ?


You need to install:
- snakemake >= 7.18.2
- python >= 3.6

In addition to above tools, you need to install pipeline-related tools:

- cutadapt
- alienTrimmer
- fastqc
- sortmeRNA
- fastq_screen
- samtools
- bowtie2
- bedtools
- star 
- minimap2
- kallisto
- salmon
- subread
- deeptools
- multiqc

For Institut Pasteur HPC users, these tools are not required, they will be automatically loaded via module during pipeline running via the snakemake `--use-envmodules` instruction.

# How to run RNAflow ?

## Usage

* **Step 1: Install workflow**

If you simply want to use this workflow, download and extract the latest release.

`git clone https://gitlab.pasteur.fr/hub/rnaflow.git`


*  **Step 2: Configure workflow**

Configure the workflow according to your needs via editing the [config.yaml](https://gitlab.pasteur.fr/hub/rnaflow/-/blob/master/config/config.yaml) and [multiqc_config.yaml](https://gitlab.pasteur.fr/hub/rnaflow/-/blob/master/config/multiqc_config.yaml) files in the `config/` directory.



*  **Step 3: Execute workflow on Maestro (Institut Pasteur HPC)**

Load your conda environment. See [runme.sh](https://gitlab.pasteur.fr/hub/rnaflow/-/blob/master/runme.sh) to create the complete conda env.

`conda activate snakemake`

Test your configuration by performing a dry-run via

`snakemake -n `


run it in a Maestro using environment modules via

`sbatch -q hubbioit -p hubbioit snakemake --cluster-config config/cluster_config.json --use-envmodules --cluster "sbatch --mem={cluster.ram} --cpus-per-task={threads} -q hubbioit -p hubbioit" -j 300 --nolock`


Visualize how the rules are connected via 

`snakemake -s Snakefile --rulegraph --nolock | dot -Tsvg > rulegraph.svg`

or how the files are processed via

`snakemake -s Snakefile -j 10 --dag --nolock | dot -Tsvg > dag.svg`

*  **Step 3bis: Execute workflow on NNCR (IFB HPC)**

Load all needed tools via `module`

```
module load snakemake/6.5.0
module load cutadapt/3.1
module load fastqc/0.11.9
module load singularity sortmerna/4.2.0
module load samtools/1.9
module load bowtie2/2.4.1
module load bedtools/2.29.2
module load star/2.6 
module load minimap2/2.18
module load kallisto/0.46.2
module load deeptools/3.5.0
module load subread/2.0.1
module load multiqc/1.11
```

> Note that alienTrimmer is not installed in NNCR HPC yet.

Test your configuration by performing a dry-run via

`snakemake -n -c 1`


run it in NNCR using environment modules via

`sbatch -c 8 snakemake --cluster-config config/cluster_config.json --cluster "sbatch --mem={cluster.ram} --cpus-per-task={threads} " -j 100 --nolock --cores 8`


# Use cases

- Generic configuration

In all cases, you need to provide information on where to locate the data. Share the directory containing the fastq files along with the suffix for pairs in a regular expression ('_R[12]' or "_R[12]_001," for example). Then, specify the extension of your fastq file ('.fastq.gz' or '.fq.gz'). By default, the results are saved in a "result" directory within the rnaflow folder. Please ensure the correct path for the temporary directory and adhere to the best practices of your HPC.

```
# Absolute path of directory where fastq are stored
input_dir: /path/to/your/data
# How mate paires are written in fastq as a regular expression
input_mate: '_R[12]'
# filemame extension
input_extension: '.fastq.gz'
# directory where you want 
analysis_dir: results
# tmpdir: write tempory file on this directory (could be /tmp/ or "/pasteur/appa/scratch/LOGIN/")
tmpdir: $MYSCRATCH
```

> The mate pairs regular expression is utilized by the pipeline to to automatically detect whether the data is single or paired-end. The use of [] and the inclusion of two possible numbers are mandatory.


## Use cases for human (or Eukaryotic) experiment

- Genome information

Fill only the four first lines relative to the first genome with your eukaryotic genome. No need to fill "host_" information when host_mapping is set to no : lines will be ignored by the pipeline. In any case, no need to comment a line in the config.yaml, it will broke the pipeline.

```
genome:
    genome_directory: /pasteur/zeus/projets/p01/BioIT/Genomes
    name: hg38
    fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.fa
    gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.gff
    host_mapping: no
    host_name: hg38
    host_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.fa
    host_gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.gff
    rRNA_mapping: yes
    ribo_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_rRNA.fa  
```

- Mapping with STAR


```
adapters:
    remove: yes
    tool: alienTrimmer # could be alienTrimmer or cutadapt
    cutadapt_file: file:config/TruSeq_Stranded_RNA.fa
    alien_file: config/TruSeq_Stranded_RNA.fa
    m: 25
    mode: b
    options_cutadapt: -O 6 --trim-n --max-n 1 
    options_alien: -k 10 -m 5 -p 80  
    quality: 30
    threads: 4

star_mapping:
    do: yes
    options: "--outFilterMismatchNoverLmax 0.05 --quantMode TranscriptomeSAM"
    pass2: "all-samples"
    threads: 4


```

> Junctions could be added by sample or from all samples to second pass. For experiments with more than 30 samples, choose by-sample instead of all-samples (to avoid RAM issue)


- Counting with Salmon (EM algorithm)

```
salmon:
    do: yes
    fasta: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_cDNA.fa
    options: ""
    libtype: "A" 
    threads: 12

```

Other tools are set to 'no'


Why counting with Salmon and not featureCounts ? some informations here :

- https://www.sciencedirect.com/science/article/pii/S2001037020303032
- https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0141910#pone-0141910-t001
- https://f1000research.com/articles/4-1521/v2



## Use cases for host-pathogen experiment

- Genome information

Fill the genome section with pathogen and host genomes (fasta and gff)

```
genome:
    genome_directory: /pasteur/zeus/projets/p01/BioIT/Genomes
    name: Rabies
    fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/Rabies/Rabies.fa
    gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/Rabies/Rabies.gff
    host_mapping: yes
    host_name: hg38
    host_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.fa
    host_gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.gff
    rRNA_mapping: yes
    ribo_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_rRNA.fa  
```

- Mapping with STAR (for host) and Bowtie2 (for pathogen)


```
adapters:
    remove: yes
    tool: alienTrimmer # could be alienTrimmer or cutadapt
    cutadapt_file: file:config/TruSeq_Stranded_RNA.fa
    alien_file: config/TruSeq_Stranded_RNA.fa
    m: 25
    mode: b
    options_cutadapt: -O 6 --trim-n --max-n 1 
    options_alien: -k 10 -m 5 -p 80  
    quality: 30
    threads: 4


bowtie2_mapping:
    do: yes
    options: "--very-sensitive "
    threads: 4

star_mapping:
    do: yes
    options: "--outFilterMismatchNoverLmax 0.05"
    pass2: "all-samples"
    threads: 4


```

- Counting with Salmon for host and featureCounts for pathogen

```
salmon:
    do: yes
    fasta: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_cDNA.fa
    options: ""
    libtype: "A" 
    threads: 12

feature_counts:
    do: yes
    options: "-t gene -g ID -s 1 " 
    options_host: "-t gene -g gene_id -s 1 "
    threads: 8

```

> For differential analysis of pathogen genes, use featureCounts results on genes. For DE of host genes, use Salmon results (quant.sf file)

Other tools are set to 'no'



## Use cases for bacteria (or prokaryotic) experiment


- Genome information

Fill only the four first lines relative to the first genome with your prokaryotic genome. No need to fill "host_" information when host_mapping is set to no : lines will be ignored by the pipeline. In any case, no need to comment a line in the config.yaml, it will broke the pipeline.


```
genome:
    genome_directory: /pasteur/zeus/projets/p01/BioIT/Genomes
    name: Rabies
    fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/Rabies/Rabies.fa
    gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/Rabies/Rabies.gff
    host_mapping: no
    host_name: hg38
    host_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.fa
    host_gff_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38.gff
    rRNA_mapping: no
    ribo_fasta_file: /pasteur/zeus/projets/p01/BioIT/Genomes/hg38/hg38_rRNA.fa  
```

- Mapping with Bowtie2 only (no introns)


```
adapters:
    remove: yes
    tool: alienTrimmer # could be alienTrimmer or cutadapt
    cutadapt_file: file:config/TruSeq_Stranded_RNA.fa
    alien_file: config/TruSeq_Stranded_RNA.fa
    m: 25
    mode: b
    options_cutadapt: -O 6 --trim-n --max-n 1 
    options_alien: -k 10 -m 5 -p 80  
    quality: 30
    threads: 4


bowtie2_mapping:
    do: yes
    options: "--very-sensitive "
    threads: 4

```

- Counting with featureCounts on genes


```
feature_counts:
    do: yes
    options: "-t gene -g ID -s 1 " 
    options_host: "-t gene -g gene_id -s 1 "
    threads: 8

```

Other tools are set to 'no'




# Authors

* Rachel Legendre (@rlegendr)

# Contributors

* Adrien Pain
* Claudia Chica
* Elise Jacquemet
* Emeline Perthame
* Emmanuelle Permal
* Hélène Lopez-Maestre
* Hugo Varet
* Natalia Pietrosemoli
* Victoire Baillet
* Vincent Guillemot
