#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre, Thomas Bigot                                             #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################




rule alienTrimmer:
    input:
        fastq = adapters_input_fastq
    output:
        temp(adapters_output)
    params:
        wkdir = adapters_wkdir,
        options = adapters_options,
        adapters = config['adapters']['alien_file'],
        mode = adapters_mode,
        min = adapters_min,
        qual = adapters_qual
    singularity:
        "rnaflow.img"
    shadow: "shallow"
    threads: config['adapters']['threads']
    log:
        adapters_log
    envmodules:
        "AlienTrimmer/2.0"
    shell:
        """
        set +o pipefail

        tmp="{input}"
        infiles=($tmp)
        
        tmp="{output}"
        outfiles=($tmp) 
    
        # add options and adapter sequences
        cmd+="AlienTrimmer -a {params.adapters} -l {params.min} -q {params.qual} {params.options}"
        # paired end or single end
        if [[ ${{#infiles[@]}} -eq 2 ]];
        then
            cmd+=" -1 ${{infiles[0]}} -2 ${{infiles[1]}} -o out_paired"
        else
            cmd+=" -i ${{infiles[0]}} -o out_single"
        fi
        #run command
        eval "${{cmd}} > {log}"
        
        #rename output files
        if [[ ${{#infiles[@]}} -eq 2 ]];
        then
            mv out_paired.1.fastq ${{outfiles[0]}}
            mv out_paired.2.fastq ${{outfiles[1]}}
        else
            mv out_single.fastq ${{outfiles[0]}}
        fi       

        """

