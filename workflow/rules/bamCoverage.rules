#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################



rule bamCoverage:
    input:
        bamCoverage_input
    log:
        bamCoverage_logs
    output:
        bamCoverage_output
    singularity:
        "rnaflow.img"
    params:
        options = bamCoverage_options
    envmodules:
        "deepTools"
    threads:
        config['bamCoverage']['threads']
    shell:
        """
        bamCoverage --bam {input} --outFileName {output} {params.options} --numberOfProcessors {threads} 2> {log}
        """


