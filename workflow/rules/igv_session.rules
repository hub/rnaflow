#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################


rule igv_session:
    input :
        igv_session_input
    output :
        igv_session_output 
    params:
        genome = igv_session_genome,
        wdir = igv_session_wdir,
        autoScale = igv_session_autoScale,
        normalize = igv_session_normalize
    log :
        igv_session_log_out
    params :
    run: 
        import os
        import sys
        import shutil
        import itertools


        colors= ["135,86,146", "243,132,0", "161,202,241", "190,0,50", "243,195,0", 
                "194,178,128", "132,132,130", "0,136,86", "230,143,172", "0,103,165", 
                "249,147,121", "96,78,151", "46,166,0", "179,68,108", "220,211,0", 
                "136,45,23", "141,182,0", "101,69,34", "226,88,34", "43,61,38"]
        


        # get real path of xml output file
        output = os.path.realpath(output[0])

        with open(output, 'w') as f:
            sys.stdout = f 
            #print header of XML
            blockcode="<Session genome=\"%s\" hasGeneTrack=\"true\" hasSequenceTrack=\"true\" nextAutoscaleGroup=\"7\" path=\"%s\" version=\"8\">"
            print(blockcode % (params.genome, output))

            #print list of files in resources
            print("<Resources>")
            for file in input:
                # copy files in IGV directory
                try:
                    shutil.copy2(file, params.wdir)
                except shutil.SameFileError:
                    pass
                
                # get id and print ressources
                id = os.path.basename(file)
                blockcode="<Resource path=\"%s\"/>"
                print(blockcode % (id))
            print("</Resources>")

            #print custom panels
            print("<Panel height=\"1180\" name=\"DataPanel\" width=\"3077\">")

            idx = 0
            for file in input:
                id = os.path.basename(file)
                name = id.split(".bw")[0]
                idx += 1 
                blockcode = "<Track altColor=\"%s\" autoScale=\"%s\" clazz=\"org.broad.igv.track.DataSourceTrack\" color=\"%s\" displayMode=\"COLLAPSED\" featureVisibilityWindow=\"-1\" fontSize=\"10\" id=\"%s\" name=\"%s\" normalize=\"%s\" renderer=\"BAR_CHART\" sortable=\"true\" visible=\"true\" windowFunction=\"mean\">"
                print(blockcode % (colors[idx], params.autoScale, colors[idx], id, name, params.normalize))
                print("<DataRange baseline=\"0.0\" drawBaseline=\"true\" flipAxis=\"false\" maximum=\"100.808\" minimum=\"0.0\" type=\"LINEAR\"/>")
                print("</Track>")
  

            print("</Panel>")
            print("</Session>")




