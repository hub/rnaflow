#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################



rule bowtie2_mapping:
    input:
        fastq = bowtie2_mapping_input,
        index = bowtie2_mapping_index_done
    output:
        sort = bowtie2_mapping_sort,
        bam = temp(bowtie2_mapping_bam)
    singularity:
        "rnaflow.img"
    log:
        err = bowtie2_mapping_logs_err,
        out = bowtie2_mapping_logs_out
    params:
        prefix_index = bowtie2_mapping_prefix_index,
        options = bowtie2_mapping_options,
        prefix = temp(bowtie2_mapping_sortprefix)
    threads:
        config["bowtie2_mapping"]["threads"]
    envmodules:
        "bowtie2",
        "samtools"
    shell:
        """
        set +o pipefail
	    tmp="{input.fastq}"
        infiles=($tmp)

        cmd="bowtie2 -p {threads} {params.options} -x {params.prefix_index}"
        # paired end or single end
        if [[ ${{#infiles[@]}} -eq 2 ]]
        then
            bowtie_input="-1 ${{infiles[0]}} -2 ${{infiles[1]}}"
        else
            bowtie_input="-U ${{infiles[0]}} "
        fi

        cmd+=" ${{bowtie_input}}"
        # sam to bam
        cmd+=" | samtools view -Sbh - > {output.bam}"

        # logs
        cmd="(${{cmd}}) > {log.out} 2> {log.err}"

        # sort result
        cmd+=" && samtools sort -o {output.sort} {output.bam} "
        cmd+=" && samtools index {output.sort}" 

        #run command
        eval "${{cmd}}"
        """
