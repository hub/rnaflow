#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################



rule feature_counts:
    input:
        bam = feature_counts_input,
        gff = feature_counts_gff
    output:
        feature_counts_output_count

    singularity:
        "rnaflow.img"
    params:
        mapp = feature_counts_options
    log:
        err = feature_counts_logs_err,
        out = feature_counts_logs_out
    threads:
        config['feature_counts']['threads']
    envmodules:
        "subread"
    shell:
        """
        featureCounts -T {threads} {params.mapp} -a {input.gff} -o {output} {input.bam} > {log.out} 2> {log.err}
        """

      
