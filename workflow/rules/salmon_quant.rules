#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################



rule salmon_quant:
    input:
       transcript = salmon_quant_transcript,
       bam = salmon_quant_bam
    output:
        directory(salmon_quant_output_dir)
    params:
        options = salmon_quant_options,
        libtype = config['salmon']['libtype']
    singularity:
        "rnaflow.img"
    log:
        err = salmon_quant_log_err,
        out = salmon_quant_log_out
    threads:
        config['salmon']['threads']
    envmodules:
        "salmon/1.9.0"
    shell:
        """
        salmon quant --threads {threads} --alignments {input.bam} --targets {input.transcript} --libType {params.libtype} --output {output} > {log.out} 2> {log.err}
        """ 
