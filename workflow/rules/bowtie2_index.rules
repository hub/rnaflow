#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################




rule bowtie2_index:
    input:
        bowtie2_index_fasta
    output:
        bowtie2_index_output_done
    singularity:
        "rnaflow.img"
    params:
        prefix = bowtie2_index_output_prefix
    log:
        bowtie2_index_log
    envmodules:
        "bowtie2",
        "samtools"
    shell:
        """
        set +o pipefail

        #compute index
        bowtie2-build {input} {params.prefix}  2> {log}
        samtools faidx {input} 2>> {log}

        """
