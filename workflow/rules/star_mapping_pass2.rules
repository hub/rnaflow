#########################################################################
# RNAflow: an automated pipeline to analyse transcriptomic data         #
#                                                                       #
# Authors: Rachel Legendre                                              #
# Copyright (c) 2021-2022  Institut Pasteur (Paris).                    #
#                                                                       #
# This file is part of RNAflow workflow.                                #
#                                                                       #
# RNAflow is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# RNAflow is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with RNAflow (LICENSE).                                         #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################


rule star_mapping_pass2:
    input:
        fastq = star_mapping_pass2_input,
        done = star_mapping_pass2_done,
        sjdb = star_mapping_pass2_junctions,
        bam = star_mapping_pass2_bam
    output:
        star_mapping_pass2_sort
    params:
        index = star_mapping_pass2_index,
        prefix = temp(star_mapping_pass2_output_prefix),
        RG = star_mapping_pass2_read_groups,
        kwargs = config['star_mapping']['options']
    singularity:
        "rnaflow.img"
    threads:
        config['star_mapping']['threads']
    envmodules:
        "STAR",
        "samtools"
    log:
        star_mapping_pass2_logs
    shell:
        """ 
        if [[ {input.fastq[0]} =~ \.gz$ ]]
        then
            STAR --genomeDir {params.index} \
                 --readFilesIn {input.fastq}  \
                 --runThreadN {threads} \
                 --genomeLoad NoSharedMemory \
                 --outSAMtype BAM SortedByCoordinate \
                 --readFilesCommand zcat \
                 --seedSearchStartLmax 20 \
                 --outFileNamePrefix {params.prefix} \
                 --outSAMattrRGline {params.RG} \
                 --sjdbFileChrStartEnd {input.sjdb} \
                {params.kwargs}  2> {log}
        else
            STAR --genomeDir {params.index} \
                 --readFilesIn {input.fastq}  \
                 --runThreadN {threads} \
                 --genomeLoad NoSharedMemory \
                 --outSAMtype BAM SortedByCoordinate \
                 --seedSearchStartLmax 20 \
                 --outFileNamePrefix {params.prefix} \
                 --outSAMattrRGline {params.RG} \
                 --sjdbFileChrStartEnd {input.sjdb} \
                {params.kwargs}  2> {log}
        fi


        samtools index {params.prefix}Aligned.sortedByCoord.out.bam  2>> {log}  
        
        """
