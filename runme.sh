# for Hubbers

#install conda via miniconda :
follow this tuto : https://docs.conda.io/en/latest/miniconda.html#linux-installers

#create your conda env
conda create -n snakemake 
conda activate snakemake
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
conda install -c anaconda python
conda install -c bioconda snakemake 
conda install -c anaconda pandas
conda install -c bioconda pysam

#run the workflow:
## DEPRECATED, follow the readme.md
conda activate snakemake
module load graalvm/ce-java8-20.0.0 fastqc
module load samtools/1.10
module load bedtools/2.29.2
module load cutadapt-devel/2.10 cutadapt/2.10
module load bowtie2/2.3.5.1
module load STAR/2.7.3a
module load subread/2.0.0
module load deepTools/3.4.1
module load MultiQC/1.9

sbatch -q hubbioit -p hubbioit snakemake --cluster-config config/cluster_config.json --cluster "sbatch --mem={cluster.ram} --cpus-per-task={threads} -q hubbioit -p hubbioit" -j 300 --nolock

#run the workflow without loading modules before (with --use-envmodules): 
conda activate snakemake
#check the pipeline:
snakemake --use-envmodules -n
#run the pipeline
sbatch -q hubbioit -p hubbioit snakemake --cluster-config config/cluster_config.json --use-envmodules --cluster "sbatch --mem={cluster.ram} --cpus-per-task={threads} -q hubbioit -p hubbioit" -j 300 --nolock


#re-run the pipeline at specific step
sbatch -q hubbioit -p hubbioit snakemake --cluster-config config/cluster_config.json --use-envmodules --cluster "sbatch --mem={cluster.ram} --cpus-per-task={threads} -q hubbioit -p hubbioit" -j 300 --nolock --forcerun multiqc

